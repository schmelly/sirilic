xgettext --language=Python --keyword=_ --output=./sirilic/i18n/sirilic.pot --from-code=UTF-8 ./sirilic/*.pyw ./sirilic/lib/*.py
msginit --input=./sirilic/i18n/sirilic.pot --output=./sirilic/i18n/fr_FR/LC_MESSAGES/sirilic.po

xgettext --language=Python --keyword=_  --output=./sirilic/i18n/sirilic.pot --from-code=UTF-8 ./sirilic/*.pyw ./sirilic/lib/*.py ./sirilic/ui/gui.py
msgmerge --update --no-fuzzy-matching --backup=off ./sirilic/i18n/fr_FR/LC_MESSAGES/sirilic.po ./sirilic/i18n/sirilic.pot	
msgfmt ./sirilic/i18n/fr_FR/LC_MESSAGES/sirilic.po --output-file ./sirilic/i18n/fr_FR/LC_MESSAGES/sirilic.mo

# -*- coding: UTF-8 -*-
# ==============================================================================

# ==============================================================================
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ==============================================================================
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ==============================================================================
DATE_VERSION="October 2022"
NO_VERSION="1.14.4x"
CHANGELOG="""CHANGELOG: 
    V""" + NO_VERSION + """
      + change about dialogbox
      + fix issue 18
      + suppress pylint warnings
    V1.14.3
      + fix archlinux compatibility
      + GUI refactoring
    V1.14.2
      + fix 'norot' register option (no prefix)
    V1.14.1
      + fix register options with multi-session
    V1.14.0
      + add register options: -transf, -norot, -minpairs
      + stop the script if there is an empty session
      + add the synthetic bias
      + delete 'hcompress' option for the compression    
    ...
"""

"""
    V1.13.11
      + add scroll bars for low resolution screens
      + add "MAD" rejection type
      + fix linux gui
    V1.13.10
      + fix Issue #13: no debayer with cosme in  Ha or HaO3 filter (DLSR) 
    V1.13.9
      + fix range for "generalized" and "percentile" rejection
    V1.13.8
      + add ExtractGreen
      + add option "weighted"
    V1.13.7
      + add a 'Siril' button in the log tab
      + delete flip option
      + Correction of the log display when no image is selected
      + fix namepath with a simple quote
      + add option "rejection type"
      + add option "identical parameters for all layers"
      + fix remove_file() with symbolic link on linux when the source file is deleted
    V1.13.6
      + fix remove_file() with symbolic link
    V1.13.5
      + add linear_match to Duo Band (HaO3)
    V1.13.4
      + fix project history 
      + fix GetHDU() with compressed FITS
      + fix DuoBand HaO3  with subsky option
    V1.13.3
      + add an abort when the sequence contains an image only
      + fix objectname with space
    V1.13.2
      + fix the reading of the FITS file header (bitpix < 0 if float32)
      + fix remove_file()
    V1.13.1
      + fix version number check : siril 0.99.8.1-91f54865
    V1.13.0
      + refactoring for siril 0.99.8
      + add fix fujiX"
      + add processing summary
      + add search field in the log tab
      + add subsky sequence
      + add single-multi fit / ser mode
      + add ser processing
      + optimisation code
      + generic script without absolute path
      + fix Sirilic Step by Step (debug)
      + fix reverse project
"""

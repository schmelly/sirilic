# -*- coding: UTF-8 -*-
# ==============================================================================

# ==============================================================================
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ==============================================================================
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ==============================================================================

import sys
import os
import io

from sirilic.lib.tools import init_language
from sirilic.lib.tools import mkdirs

init_language()
from sirilic.lib.constantes import COMPATIBILITY_SIRIL
from sirilic.lib.constantes import DSLR,RGB,IMAGE,OFFSET,DARK,FLAT,DFLAT,NB_IODF,TYPENAME
from sirilic.lib.constantes import MASTERDIR,TEMPDIR,GROUPDIR,SRCDIR
from sirilic.lib.constantes import NoLayer
# ==============================================================================

# global counter
StepCounter=0

# ==============================================================================
class CSsfWriter:
    def __init__(self):
        self.fd = None
    # --------------------------------------------------------------------------
    def write(self,  chaine ):
        try:
            if sys.platform.startswith('win32')  :
                # conversion des accents locale en utf8 pour le script
                #code=locale.getpreferredencoding(True)
                chaine=chaine.rstrip('\r')
                #bchaine = chaine.encode(errors='ignore')
                #chaine = bchaine.decode(code,errors='ignore')
            self.fd.write( chaine )
        except Exception as e :
            print("error CSsfWriter::write() : " + str(e) )

    def comment(self, text="",space=" " ):
        self.write("#"+space + text +"\n")

    def command(self, cmd="",arguments=" " ):
        self.write(cmd + " " + arguments +"\n")

    def hline(self, car='#',space='' ):
        cnt=80-1-len(space)
        self.write("#"+ space + car*cnt +"\n")

    def title(self, text,cr=0):
        #self.comment()
        self.comment(text)
        #self.comment()
        self.cr(cr)

    def cr(self, cnt=1 ):
        if cnt >0: self.write("\n"*cnt)

# ==============================================================================
class CBasicSirilScript(CSsfWriter):
    def __init__(self, log, sirilprefs , db_images, glob_prefs):
        self.log     = log
        self.prefs   = sirilprefs
        self.db      = db_images
        self.props   = glob_prefs

        self.extDest       = ".fit"
        self.dev           = self.prefs.Get('siril_dev')
        self.avancement    = ""
        self.compatibility = COMPATIBILITY_SIRIL[self.dev].strip()
        self.compatibility = ".".join(self.compatibility.split('.')[0:3])
        self.quote= '"'
    # --------------------------------------------------------------------------

    def chdir(self, dirname=".", cmnt="", cr=0 ):
        if len(cmnt)>0 : self.comment(cmnt)
       
        self.write('cd '+ self.quote + dirname.replace('\\','/') + self.quote +"\n")
        self.cr(cr)

    def updir(self,cr=0 ):
        self.chdir("..",cr=cr)

    def copy(self,dst,src ):
        self.command("load", self.quote + src + self.quote )
        self.command("save", self.quote + dst + self.quote )

    def copy_multiple(self,list_dst,src ):
        self.command("load", self.quote + src + self.quote )
        for dst in list_dst : self.command("save", self.quote + dst  + self.quote )

    def savetif(self,imagename,cond=True ):
        if cond is True : self.command("savetif", self.quote + imagename + self.quote )

    def init_progress(self, workdir,objectname,layername,session):
        self.avancement = os.path.join(objectname,layername,session)
        self.imagedirabs= os.path.join(workdir,self.avancement)
        self.imagedir   = os.path.join(".", self.avancement)
        self.avancement = self.avancement + ": "

    def progress( self, step_cur, maj=True):
        global StepCounter
        StepCounter = StepCounter + 1
        step_new = self.avancement + step_cur
        self.write("#TAG#{ ["+str(StepCounter)+ "] " + step_new + " ...\n")
        self.comment()
        if maj : self.avancement = step_new  + ", "

    def end_progress(self):
        global StepCounter
        self.write("#\n#TAG#} ["+str(StepCounter)+ "]\n\n")

    def getStepNumber(self):
        global StepCounter
        return StepCounter

    def ClearStepNumber(self):
        global StepCounter
        StepCounter=0

    def isBayerMatrix(self, layername ):
        return (layername == DSLR) or (layername == RGB) or (layername == "cHa") or (layername == "cHaO3") or (layername == "cGreen")

    def isDeBayerised(self, layername ):
        return ( (layername == "dL") or (layername == "dR") or (layername == "dG") or (layername == "dB") or
                 (layername == "dHa") or (layername == "dHb") or (layername == "dS2") or (layername == "dO3")  )

    def isMono(self, layername ):
        return ( (layername == "L") or (layername == "R") or (layername == "G") or (layername == "B") or
                (layername == "Ha") or (layername == "Hb") or (layername == "S2") or (layername == "O3")  )

    def isDSLR_Raw(self, layername):
        cond = (layername == DSLR) or (layername == "cHa") or (layername == "cHaO3") or (layername == "cGreen") or self.isDeBayerised(layername )
        return cond

    # --------------------------------------------------------------------------
    def header_file(self ,nom_script):
        with io.open(nom_script, "w", newline='', encoding='utf-8') as self.fd :
            self.hline('#')
            self.title( _("Don't edit : generated by ") + sys.argv[0] )
            self.title(_("Compatibility with") + " Siril: Version >= " + self.compatibility )
           
            self.command('requires ' + self.compatibility )
            self.cr()

            self.comment(_("uncomment next line to force") + " " + _("Work Directory") )
            self.write( "# cd " + self.quote +  self.prefs.Get('workdir') + self.quote ) 
            self.cr(cnt=2)
            
            precision= '32' if self.prefs.Get('float32b') else '16'
            self.command('set' + precision + 'bits')
            self.cr()

            if self.prefs.Get('compress') :
                self.command( 'setcompress', '1 -type=' + self.prefs.Get('compress_type') + " " + str(self.prefs.Get('compress_quantif')))
            else:
                self.command('setcompress', '0')
            self.cr()

            self.command('setext',  self.extDest[1:] )
            self.cr()

            if self.prefs.Get('nbcpu') != 0 :
                self.command('setcpu' , str(self.prefs.Get('nbcpu')) )
                self.cr()
            if self.prefs.Get('nbmem') != 0 :
                self.command('setmem' , str(self.prefs.Get('nbmem')) )
                self.cr()

    # --------------------------------------------------------------------------
    def mk_stack_param( self, iodf ) :
        if not iodf["files"] : return ""
        stack_param=" " + iodf['Stack'] + " "
        if iodf['Stack'] == "rej" or  iodf['Stack'] == "mean": 
            rejtype = iodf['RejType'][0].lower()
            if iodf['RejType'].lower() == "mad" : rejtype = "a"
            stack_param += rejtype + " " + str(iodf['RejL']) + " " + str(iodf['RejH']) + " "
        stack_param += "-weighted " if iodf['Weighted'] == True else ""
        stack_param += "-nonorm "   if iodf['Norm'] == "no" else  "-norm=" + iodf['Norm'] + " "
        if iodf['type'] == IMAGE :
            if self.check_param_filt(iodf['fwhm']  ,  20.0) : stack_param += "-filter-fwhm="    + iodf['fwhm']    + " "
            if self.check_param_filt(iodf['wfwhm'] ,  20.0) : stack_param += "-filter-wfwhm="   + iodf['wfwhm']   + " "
            if self.check_param_filt(iodf['round']  ,  1.0) : stack_param += "-filter-round="   + iodf['round']   + " "
            if self.check_param_filt(iodf['quality'],100.0) : stack_param += "-filter-quality=" + iodf['quality'] + " "
        return stack_param
    
    def mk_reg_param(self, image ):
        register_opt =""
        if image['Transf']:
            register_opt +=" -transf=" + image['Transf'] +" "
        if image['MinPairs']:
            register_opt +=" -minpairs=" + image['MinPairs'] +" "
        if image['NoRot']:
            register_opt +=" -norot " 
        return register_opt

    # --------------------------------------------------------------------------
    def check_param_filt( self, value,  val_max ) :
        if value ==   None : return False
        if value ==   "0%" : return False
        if value == "100%" : return False
        if value ==     "" : return False
        if value[-1] == "%" :
            if self.isFloat( value[:-1] ) == False : return False
            nombre = float(value[:-1])
            if nombre > 100 : return False
            if nombre <   0 : return False
        else:
            if self.isFloat( value[:-1] ) == False : return False
            nombre = float(value[:-1])
            if nombre <       0 : return False
            if nombre > val_max : return False
        return True

    # --------------------------------------------------------------------------
    def isFloat(self, value ) :
        try:
            float(value)
            return True
        except ValueError:
            return False

    # --------------------------------------------------------------------------
    def SetFindStar(self, image ) :
        if image['Findstar'] :
            self.progress( "SetFindStar",maj=False)
            self.command("setfindstar", " " + str(image['Ksigma']) + " " + str(image['Roundness']) )
            self.end_progress( )

    # --------------------------------------------------------------------------
    def preprocess( self, cond, pp, output, offset=None,dark=None,flat=None , options=""):
        if cond :
            masters=""
            if offset is not None : masters += " -bias=" + offset
            if dark   is not None : masters += " -dark=" + dark
            if flat   is not None : masters += " -flat=" + flat
            if ((masters != "") or (options != "" ) ):
                self.command("preprocess", output.lower() + masters + options )
                pp="pp_" + pp
        return pp

    # --------------------------------------------------------------------------
    def stacking( self, pp, imagename, out_imagename, stack_param ):
        self.command("stack", pp + imagename.lower() + stack_param + " -out=" + out_imagename)

# ==============================================================================
class CScriptBuilder(CBasicSirilScript):
    def __init__(self, log, nom_script, sirilprefs , db_images, glob_prefs ):
        CBasicSirilScript.__init__(self,log, sirilprefs , db_images, glob_prefs)
        self.script = nom_script

    # --------------------------------------------------------------------------
    def Build(self):
        if self.CheckInitialisedSession() : return None     
        self.ClearStepNumber()
        self.last_processed_image = ""
        self.arbre   = self.db.GetArborescence()
        self.header_file(self.script)
        self.BuildScriptPart1()
        self.Merge()
        self.BuildScriptPart2()
        return self.last_processed_image

    # --------------------------------------------------------------------------
    def CheckInitialisedSession(self):
        liste_keystr = self.db.GetKeyStrSorted()
        flag=False
        for keystr in  liste_keystr :
            if self.db.Item_IsInitialised(keystr ) == "uninitialized" :
                self.log.insert(_("Error")+ " " + keystr + " " + _("uninitialized") +'\n')
                flag=True
        if flag :
            self.log.insert(_("Info")+ " : " + _("action => intialize or delete empty sessions") +'\n')
            
        return flag
    # --------------------------------------------------------------------------
    def BuildScriptPart1(self):
        liste_keystr = self.db.GetKeyStrSorted()
        for keystr in  liste_keystr :
            script_img = CBuildImage(self.log, self.prefs , self.db, self.props,keystr)
            outputs = script_img.Build(self.script)
            if outputs is not None:
                for output in outputs:
                    self.last_processed_image = output[1]
                    xx,yy,zz = self.db.Split(keystr)
                    self.arbre[xx][yy][zz].append( output[0] )
    # --------------------------------------------------------------------------
    def BuildScriptPart2(self):
        script_multi = CMultisessionScript(self.log, self.prefs , self.db, self.props)
        last_processed_image_multi = script_multi.Build(self.script,self.arbre)
        if  last_processed_image_multi != None : self.last_processed_image = last_processed_image_multi

    # --------------------------------------------------------------------------
    def Merge(self):
        if not self.props['multisession'] : return
        try:
            arbre = self.arbre
            if len(arbre.keys()) == 0 :
                return
            # fusion ou pas ?
            fusion_flag=False
            for imgname,layers in arbre.items() :
                for layername,sessions in layers.items() :
                    if len(sessions) <2 :  continue
                    fusion_flag=True
            if not fusion_flag :
                return
            # fusion 
            with io.open(self.script, "a", newline='', encoding='utf-8' ) as self.fd :
                self.hline('#')
                self.progress( _("Merge") )
                for imgname,layers in arbre.items() :
                    for layername,sessions in layers.items() :
                        if len(sessions) <2 :  continue
                        dirpath=os.path.join(imgname,layername,GROUPDIR )
                        mkdirs( os.path.join(self.prefs.Get('workdir'),dirpath) )
                        dirpath=os.path.join("." , dirpath)
                        self.chdir( dirpath,_("Group Directory") )
                        list_file="merge "
                        for session,param in sessions.items() :
                            img = param[1] + TYPENAME[IMAGE].lower()
                            list_file = list_file + " ../" + session + "/" + TYPENAME[IMAGE] + '/' + img
                        self.command(list_file, "image_group")
                        self.chdir('../../..')
                self.end_progress( )
        except Exception as e :
            self.log.insert("***" +  _("error") + " " + _("merge") + " : " + str( e ) + '\n')


# ==============================================================================
# CONSTRUCTION DU SCRIPT  DE PRETRAITEMENT DE L'IMAGE (DARK/OFFSET/FLAT)
class CBuildImage(CBasicSirilScript):
    def __init__(self, log, sirilprefs , db_images, glob_prefs,keystr ):
        CBasicSirilScript.__init__(self,log, sirilprefs , db_images, glob_prefs)

        self.layer_is_multi = self.db.IsMultiSession(keystr)
        obj_iodf            = self.db.GetItem(keystr)
        self.objectname,self.layername,self.session = keystr.split("/")

        self.offset    = obj_iodf.GetDbIodf(OFFSET)
        self.dark      = obj_iodf.GetDbIodf(DARK)
        self.darkflat  = obj_iodf.GetDbIodf(DFLAT)
        self.flat      = obj_iodf.GetDbIodf(FLAT)
        self.image     = obj_iodf.GetDbIodf(IMAGE)

        self.bseqmode  = self.prefs.Get('seqmode')
        self.bSavetif  = self.prefs.Get('savetif')
        self.workdir   = self.prefs.Get('workdir')
        self.multi     = self.props['multisession']

        self.ena_stack = (    (self.multi == False               )
                           or (self.props['stack_intermed']==True)
                           or (self.layer_is_multi == False      ) )

        self.ena   =[obj_iodf.GetIodf(ii).IsInitialised() != '' for ii in range(NB_IODF)]
        self.enaLib=[obj_iodf.GetIodf(ii).IsLib() for ii in range(NB_IODF)]
        self.IsMultiImage =[obj_iodf.GetIodf(ii).IsMultiImage() for ii in range(NB_IODF)]
        self.fix_fujix = self.image['fix_fujix']

        # PRECALCUL DES VARIABLES
        dir_master           = "../" + MASTERDIR
        self.master_offset   = dir_master + "/master-" + self.offset['typename'][:-1].lower()   + self.extDest
        self.master_dark     = dir_master + "/master-" + self.dark['typename'][:-1].lower()     + self.extDest
        self.master_darkflat = dir_master + "/master-" + self.darkflat['typename'][:-1].lower() + self.extDest
        self.master_flat     = dir_master + "/master-" + self.flat['typename'][:-1].lower()     + self.extDest
        
        if obj_iodf.GetIodf(OFFSET).IsSynthetic() : 
            self.master_offset = '"' + obj_iodf.GetIodf(OFFSET).GetValueSynthetic() + '"'

        self.seuils_cosme=" " + str(self.image['Cold']) + " " + str(self.image['Hot']) + " "
        
        self.register_opt = self.mk_reg_param(self.image)            
        self.drizzle =" -drizzle "      if self.image['Drizzle'] else ""
        cfaequa      =" -equalize_cfa " if self.props['CFAequa'] else ""
        self.cfa     =" -cfa " + cfaequa if (self.layername == DSLR) or (self.layername == RGB) else ""

        if (self.layername == DSLR) or (self.layername == RGB) :
            if not self.image['Cosmetic'] :
                self.cfa= self.cfa + " -debayer "

        if (self.layername == "cHa") or (self.layername == "cHaO3") or (self.layername == "cGreen")  :
            self.cfa=" -cfa " + cfaequa

        self.dark_opt=" -opt " if self.image['DarkOpt'] else ""
        self.init_progress(self.workdir,self.objectname,self.layername,self.session)

    # --------------------------------------------------------------------------
    # AJOUT DANS LE SCRIPT LE PRETRAITEMENT POUR UNE IMAGE
    def Build(self, nom_script):
        try:
            with io.open(nom_script, "a", newline='', encoding='utf-8' ) as self.fd :
                self.header()
                self.copylib_already_processed()
                self.offset_processing()
                self.dark_processing()
                self.darkflat_processing()
                self.flat_processing()
                out = self.light_processing()
                self.chdir('../../..')
        except:
            out=None
        return out

    # --------------------------------------------------------------------------
    def header(self):
        self.hline('#')
        self.title( _('Automating: layer ')  + self.layername + _(' of ') + self.session )
        self.cr()
        self.chdir( self.imagedir, _("Object Directory"), 1 )

    # --------------------------------------------------------------------------
    def copylib_already_processed(self): # COPIE DES LIBRAIRIES : Resultat d'un script precedent
        cond = self.offset['copylib'] and self.enaLib[OFFSET] and self.ena[OFFSET]
        self.copy_lib( cond, self.offset )

        cond = self.dark['copylib'] and self.enaLib[DARK] and self.ena[DARK]
        self.copy_lib( cond, self.dark )

        cond = self.flat['copylib'] and self.enaLib[FLAT] and self.ena[FLAT]
        self.copy_lib( cond, self.flat )

        cond = self.darkflat['copylib'] and self.enaLib[DFLAT] and self.ena[DFLAT]
        self.copy_lib( cond, self.darkflat )

    # --------------------------------------------------------------------------
    def offset_processing(self):
        cond = (not self.enaLib[OFFSET] ) and self.ena[OFFSET]
        if cond == False : return

        self.preambule( self.offset, self.IsMultiImage[OFFSET])
        stack_param = self.mk_stack_param( self.offset  )
        self.stacking( "", self.offset['typename'], self.master_offset, stack_param )

        self.postambule( )

    # --------------------------------------------------------------------------
    def dark_processing(self):
        cond = (not self.enaLib[DARK] ) and self.ena[DARK]
        if cond == False : return

        self.preambule( self.dark, self.IsMultiImage[DARK] )

        cond_pp     = (self.ena[OFFSET] and self.dark['suboffset'])
        offset_file = self.master_offset if cond_pp        else None
        pp=self.preprocess( cond_pp , "", self.dark['typename'], offset=offset_file )

        stack_param = self.mk_stack_param( self.dark  )
        self.stacking( pp, self.dark['typename'], self.master_dark, stack_param )

        self.postambule( )

    # --------------------------------------------------------------------------
    def darkflat_processing(self):
        cond = (not self.enaLib[DFLAT] ) and self.ena[DFLAT]
        if cond == False : return

        self.preambule( self.darkflat, self.IsMultiImage[DFLAT])

        cond_pp     = (self.ena[OFFSET] and self.darkflat['suboffset'])
        offset_file = self.master_offset if cond_pp  else None
        pp=self.preprocess( cond_pp , "", self.darkflat['typename'], offset=offset_file )

        stack_param = self.mk_stack_param( self.darkflat  )
        self.stacking( pp, self.darkflat['typename'], self.master_darkflat, stack_param )

        self.postambule( )

    # --------------------------------------------------------------------------
    def flat_processing(self):
        cond = (not self.enaLib[FLAT] ) and self.ena[FLAT]
        if cond == False : return

        self.preambule( self.flat, self.IsMultiImage[FLAT])

        cond            = self.ena[OFFSET] and self.flat['suboffset']
        master_offset   = self.master_offset if cond else None
        master_darkflat = self.master_darkflat if self.ena[DFLAT] else None
        cond            = cond or self.ena[DFLAT]
        pp=self.preprocess( cond , "",  self.flat['typename'], offset=master_offset, dark=master_darkflat  )

        stack_param = self.mk_stack_param( self.flat  )
        self.stacking( pp, self.flat['typename'], self.master_flat, stack_param )

        self.postambule( )

    # --------------------------------------------------------------------------
    def light_processing(self):
        cond = ( not self.enaLib[IMAGE] ) and self.ena[IMAGE]
        if cond == False : return None

        self.preambule( self.image, self.IsMultiImage[IMAGE], _("PROCESSING"))

        options_pp  = " -fix_xtrans"     if self.fix_fujix else ""

        pp=""
        master_offset = self.master_offset if self.ena[OFFSET] and self.image['suboffset'] else None
        master_dark   = self.master_dark   if self.ena[DARK]  else None
        master_flat   = self.master_flat   if self.ena[FLAT]  else None
        cond = ( (self.cfa != "" )  or (self.ena[OFFSET] and self.image['suboffset'] )
                 or self.ena[DARK]  or self.ena[FLAT] )

        pp=self.preprocess( cond, pp,  self.image['typename'],
                           offset=master_offset, dark=master_dark, flat=master_flat,
                           options= self.cfa + self.dark_opt + options_pp )

        pp=self.cosmetic( pp )

        self.postambule( )

        output=[]
        self.last_processed_image=""
        if self.ena_stack :
            self.SetFindStar(self.image )

            duo_name = ''
            typename=self.image['typename'].lower()
            if (self.layername == "cHa") :
                self.progress( "extract Ha", maj=False)
                self.chdir( self.image['typename'] )
                self.command("seqextract_Ha", pp + typename )
                self.updir()
                pp="Ha_" + pp
                self.drizzle=" -drizzle "
                self.end_progress( )
            if (self.layername == "cGreen") :
                self.progress( "extract Green", maj=False)
                self.chdir( self.image['typename'] )
                self.command("seqextract_Green", pp + typename )
                self.updir()
                pp="Green_" + pp
                self.drizzle=" -drizzle "
                self.end_progress( )
            if (self.layername == "cHaO3")  :
                self.progress( "extract Ha-Oiii", maj=False)
                self.chdir( self.image['typename'] )
                self.command("seqextract_HaOIII", pp + typename )
                self.updir()
                self.drizzle=" -drizzle "
                self.end_progress( )
                self.pp1,self.last_processed_image= self.register_stack( pp, 1, "Ha" )
                if not self.multi :
                    self.pp1 = None
                output.append([self.pp1, self.last_processed_image + self.extDest])
                duo_name = "OIII"

            pp,self.last_processed_image = self.register_stack( pp, 0, duo_name )

        self.cr()
        if not self.multi : pp = None
        last_processed_ext=None
        if self.last_processed_image != None : last_processed_ext=self.last_processed_image + self.extDest
        output.append([pp,last_processed_ext])
        return output

    # --------------------------------------------------------------------------
    def preambule(self, iodf, IsMultiImage=False, msg = _("MASTER BUILDING") ):
        name=iodf['typename'][:-1]
        self.progress( name.lower())
        self.title(name + " " + msg )
        self.chdir(iodf['typename'])
        if not IsMultiImage :
            self.convert_fit( iodf['typename'] )

    # --------------------------------------------------------------------------
    def postambule(self):
        self.updir()
        self.end_progress( )

    # --------------------------------------------------------------------------
    def register_stack( self, pp, no_duo, duo_name):
        last_processed_image=None
        layername = self.layername
        noLayer   = NoLayer[layername] - no_duo            
        sep=""
        if duo_name != "" :
            layername="c"+duo_name
            pp = duo_name + "_" + pp
            sep="."
    
        prefixOut= "../"
        if duo_name == "OIII" : 
            layername = "cO3" 
            prefixOut= ""
            
        self.progress( "register" + sep + duo_name)
        self.chdir( self.image['typename'] )
        typename=self.image['typename'].lower()
        if self.image["Degree"] != 0 :
            self.command("seqsubsky", pp + typename + " " +  str(self.image["Degree"] ))
            pp = 'bkg_' + pp
        self.command("register", pp + typename + self.drizzle + self.register_opt )
        self.updir()
        self.end_progress( )
        
        pp_r="r_" if self.register_opt.find('-norot') == -1  else ""
                    
        self.progress( "stack" + sep + duo_name)
        self.chdir( self.image['typename'] )
        stack_param = self.mk_stack_param( self.image )
        if duo_name != "" : stack_param += " -output_norm" 
        self.command("stack", pp_r + pp + typename + stack_param + " -out=" + prefixOut +  layername )
        if duo_name == "OIII" : 
            self.comment(_("Linear match") + ": " + layername + " / " +"cHa")
            self.command("load", layername )
            self.command("linear_match", "../cHa 0 0.92" )
            self.command("save", "../" +  layername )

        self.comment(_("Copy") + " " + layername + " " + _("to") +" C%02d" % (noLayer,))
        basedir="../../../"
        if self.layer_is_multi is True :
            session= "-" + self.session # evite les ecrasements
        else:
            session= ""
            last_processed_image= os.path.join(self.workdir, self.objectname,  self.objectname + "_" + layername  )

        names=( basedir + TEMPDIR + "/C%02d" % (noLayer,) +  session,
                basedir + self.objectname + "_"  + layername + session )
        self.copy_multiple(src="../" + layername,list_dst=names)
        self.savetif( basedir + self.objectname + "_"  + layername +  session, self.bSavetif )
        self.updir()
        self.end_progress( )

        return pp,last_processed_image

    # --------------------------------------------------------------------------
    def copy_lib( self, cond, lib ):
        if cond :
            libname  = lib['files'][0].replace('\\','/')
            typename = lib['typename'][:-1]
            mastername = "master-" +  typename.lower() + self.extDest

            self.comment(_("COPY THE")+ " " + typename + " "
                         + _("LIBRARY INTO THE MASTER DIRECTORY"))
            self.chdir(MASTERDIR )
            self.copy(src=libname, dst=mastername)
            self.updir()

    # --------------------------------------------------------------------------
    # Conversion des images en fit
    def convert_fit( self, imagename ):
        options=' -out=..'
        if self.bseqmode == 1 : options = " -fitseq" + options
        if self.bseqmode == 2 : options = " -ser"    + options
        self.command("cd " + SRCDIR)
        self.command("convert", imagename.lower() + options )
        self.updir()

    # --------------------------------------------------------------------------
    def cosmetic(self, pp ):
        if self.image['Cosmetic'] :
            typename=self.image['typename'].lower()
            if self.isBayerMatrix(self.layername) :
                self.command("seqfind_cosme_cfa",  pp + typename + self.seuils_cosme )
                param="cc_"+ pp + typename 
                if (self.layername == DSLR) or (self.layername == RGB) : 
                    param+= " -debayer "   # no debayer : Ha , HaO3 or Green
                self.command("preprocess", param )                        
                pp = "pp_cc_" + pp
            else:
                self.command("seqfind_cosme", pp + typename + self.seuils_cosme)
                pp = "cc_" + pp
        return pp

# ==============================================================================
# CONSTRUCTION DU SCRIPT MULTISESSION D'UNE IMAGE ET ALIGNEMENT DES COUCHES COULEUR
class CMultisessionScript(CBasicSirilScript):
    def __init__(self, log, sirilprefs , db_images, glob_prefs ):
        CBasicSirilScript.__init__(self,log, sirilprefs , db_images, glob_prefs)

    # --------------------------------------------------------------------------
    def Build(self, nom_script,arbre):
        workdir      = self.prefs.Get('workdir')
        retworkdir   = self.prefs.Get('workdirreturn')
        bSavetif     = self.prefs.Get('savetif')
        multisession = self.props['multisession']
        
        returndir    = workdir 

        last_processed_image=None

        with io.open(nom_script, "a", newline='', encoding='utf-8') as self.fd :
            self.hline('#')
            for objectname,layers in  arbre.items() :
                ccd_mono = 0
                no_align = False
                for layername,sessions in layers.items():
                    if not ((layername==DSLR) or (layername==RGB) ) :
                        ccd_mono = ccd_mono +1
                    if (len(sessions) != 1) and ( multisession is False ):
                        no_align = True
                    if (len(sessions) == 1) or ( multisession is False ):
                        continue
                    for values in sessions.values() :
                        keystr   = values[0]
                        obj_iodf = self.db.GetItem(keystr)
                        image    = obj_iodf.GetDbIodf(IMAGE)
                        break

                    stack_param = self.mk_stack_param( image )
                    self.seuils_cosme=" " + str(image['Cold']) + " " + str(image['Hot']) + " "
                    self.subsky = image["Degree"]
                    
                    self.register_opt = self.mk_reg_param(image)            
                    drizzle=" -drizzle " if image['Drizzle'] else ""

                    imagename="image_group"
                    if self.prefs.Get('seqmode') == 0 : imagename += "_"

                    self.init_progress(workdir,objectname,layername,GROUPDIR)
                    self.hline('#')
                    self.chdir(self.imagedir, _("Image Directory"),cr=1)

                    self.SetFindStar(image )

                    pp       = ''
                    duo_name = ''
                    if (layername == "cHa") :
                        self.progress( "extract Ha", maj=False)
                        self.command("seqextract_Ha", imagename )
                        imagename="Ha_" + imagename
                        drizzle=" -drizzle "
                        self.end_progress( )
                    if (layername == "cHaO3")  :
                        self.progress( "extract Ha-Oiii", maj=False)
                        self.command("seqextract_HaOIII", imagename )
                        drizzle=" -drizzle "
                        self.end_progress( )
                        self.register_stack_grp(objectname,layername,"Ha_" + imagename, drizzle, stack_param , 1 , "Ha" )
                        pp="OIII_"
                        duo_name="O3"
                    last_processed_image = self.register_stack_grp(objectname,layername,pp + imagename, drizzle, stack_param , 0, duo_name )
                    self.chdir("../../..")

                imagedir=os.path.join(".",objectname)
                self.avancement = objectname + " : "
                if (ccd_mono > 1) and (no_align is False) :
                    self.chdir(imagedir+ "/" + TEMPDIR,_("Object Directory"),cr=1)
                    self.comment(_("Register CCD Layers"))

                    self.progress("Register CCD Layers")
                    self.command("register", "C")
                    for layername in layers.keys():
                        last_processed_image="final_" + objectname + "_" + layername
                        noLayer = NoLayer[layername]
                        self.copy(src="r_C%02d" % (noLayer,), dst="../" + last_processed_image )
                        self.savetif("../"+ last_processed_image,bSavetif)
                        last_processed_image = os.path.join(imagedir, last_processed_image  + self.extDest)
                    self.end_progress( )
                    self.chdir("../..")
                returndir = imagedir

            self.avancement = "... "
            self.progress( _("Finished") + " " )
            
            returndir=os.path.join(".", os.path.basename(returndir) )
            cmntdir=_("Object Directory")
            if retworkdir :
                returndir = workdir
                cmntdir=_("Work Directory")                
            self.cr()
            self.chdir( returndir,cmntdir,cr=1 )
                
            self.end_progress( )
            
            if last_processed_image is not None :
                last_processed_image=last_processed_image.replace('\\','/')
                self.comment(_("Last processed image") + ":" + last_processed_image)

            self.command("close")
            return last_processed_image

    # --------------------------------------------------------------------------
    def register_stack_grp(self, objectname, layername,imagename, drizzle, stack_param , no_duo, duo_name ):
        sep=""
        if duo_name != "" : sep="." 
        self.progress( "register" + sep + duo_name)
        pp= ''
        if self.subsky != 0 :
            self.command("seqsubsky", imagename + " " + str(self.subsky))
            pp = 'bkg_' + pp
        self.command("register", pp + imagename + drizzle + self.register_opt )
        
        if self.register_opt.find('-norot') == -1 : pp = "r_" + pp 
        self.end_progress( )

        self.progress( "stack" + sep + duo_name)
        noLayer = NoLayer[layername] - no_duo
        
        prefixOut= "../"
        if duo_name == "O3" : 
            prefixOut= ""
            
        stackname=layername
        if duo_name != "" :
            stack_param += " -output_norm" 
            stackname = 'cGroup' + duo_name
  
        self.command("stack", pp + imagename + stack_param + " -out=" + prefixOut + stackname  )
        if duo_name == "O3" : 
            self.comment(_("Linear match") + ": " + stackname + " / " +"cHa")
            self.command("load", stackname )
            self.command("linear_match", "../cGroupHa 0 0.92" )
            self.command("save", "../" +  stackname )

        self.comment(_("Copy") + " " + stackname + " " + _("to") +" C%02d" % (noLayer,) )
        basedir="../../"
        names=( basedir + objectname + "_" + stackname ,
                basedir + TEMPDIR + "/C%02d" % (noLayer,) )
        self.copy_multiple(src="../" + stackname, list_dst=names)
        last_processed_image = os.path.join(self.imagedir, names[0]  + self.extDest)
        self.end_progress( )
        return last_processed_image


#! /usr/bin/env python

# -*- coding: UTF-8 -*-

# ------------------------------------------------------------------------------
# Project: SiriL-ic ( SiriL Image Converter )
#
# This script structures the SiriL work folder into a subfolder, copies the
# astronomical images into the subfolders, and builds the associated SiriL script.
# It can also group scripts.
# ------------------------------------------------------------------------------
#    Author:  M27trognondepomme <pebe92 (at) gmail.com>
#
# This program is provided without any guarantee.
#
# The license is  LGPL-v3
# For details, see GNU General Public License, version 3 or later.
#                        "https://www.gnu.org/licenses/gpl.html"
# ------------------------------------------------------------------------------

import wx
import os
import sys

# ------------------------------------------------------------------------------
# Check Mode Developement - Sirilic package is not installed
IsPyInstallerBundle= getattr(sys, 'frozen', False) and hasattr(sys, '_MEIPASS')
SirilicExist="sirilic" in sys.modules
if not SirilicExist and not IsPyInstallerBundle :
    from pathlib import Path
    file = Path(__file__).resolve()
    try:
        sys.path.remove(str(file.parent))
    except ValueError: # Already removed
        pass
    sys.path.append(str(file.parents[1]))
    print("Info: Mode Developement - Sirilic package is not installed")    
# ------------------------------------------------------------------------------
 
from sirilic.lib import tools,callbacks,database,prefs
from sirilic.ui  import gui
    
# ==============================================================================
class App(wx.App):
    def OnInit(self):
        # Configuration
        varname     = 'USERPROFILE' if os.name == 'nt' else 'HOME'
        self.myhome = os.environ.get(varname)
        i_prefs     = prefs.CPrefs( self.myhome , '.sirilic2_rc' )
        i_db        = database.CDatabase()
        i_gui       = gui.CGui(None, -1, "") # instance Sirilic GUI
        self.cb     = callbacks.CCallbacks(i_gui,i_db, i_prefs )

        # add icon
        iconname     = os.path.join(".","icon","cp-nb.ico")
        iconpath     = os.path.dirname(os.path.abspath(__file__))
        iconfullname = tools.resource_path(iconname, pathabs=iconpath )
        bitmap       = wx.Bitmap(iconfullname, wx.BITMAP_TYPE_ICO)
        _icon        = wx.NullIcon
        _icon.CopyFromBitmap(bitmap)
        i_gui.SetIcon(_icon)

        i_gui.Show(True)
        self.SetTopWindow(i_gui)

        i_gui.SetSize((1250,1000)) # linux
        if sys.platform.startswith('win32') :
            i_gui.SetSize((1000,925))

        return True

## -----------------------------------------------------------------------------
def Run():
    if sys.version_info[0] != 3 :
        print( "*")
        print( "* Python version :", sys.version_info )
        print( "* Error, python version should be >= 3 " )
        print( "*")
        exit()
    app = App(0);
    app.MainLoop()

if __name__ == '__main__':
    Run()
